//
//  TestHelpers.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "TestHelpers.h"

NSDictionary * _TestHelpersNotificationDictionary(void) {
    return @{
             @"body" :              @"test",
             @"creation_date":      @1383811200,
             @"id":                 @"b19a058c-9g6f-9165-7cf8-ef6972442372",
             @"level":              @"warning",
             @"modification_date":  @1383811200,
             @"subject":            @"Test Subject"
             };
}

#pragma mark -
@implementation TestHelpersCoreDataStack
@end

#pragma mark -
@implementation TestHelpers

+ (TestHelpersCoreDataStack *)inMemoryCoreDataStack {
    TestHelpersCoreDataStack *stack = [[TestHelpersCoreDataStack alloc] init];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"FetcherTest" withExtension:@"momd"];
    stack.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:url];
    stack.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:stack.managedObjectModel];
    
    if (![stack.persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:nil]) {
        return nil;
    }
    
    stack.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    stack.managedObjectContext.persistentStoreCoordinator = stack.persistentStoreCoordinator;
    
    return stack;
}

+ (NSManagedObjectContext *)inMemoryContext {
    return [self inMemoryCoreDataStack].managedObjectContext;
}

@end
