//
//  TestHelpers.h
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

/*!
 Return a test @c Notiifcation formatted @c NSDictionary.
 
 @return an @c NSDictionary that can be used to create a test @c Notification core data object
*/
NSDictionary * _TestHelpersNotificationDictionary(void);

#pragma mark -
@interface TestHelpersCoreDataStack : NSObject
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@interface TestHelpers : NSObject
/*!
 Create and return an in memory @c TestHelpersCoreDataStack for testing
 
 Each call will return a new @c TestHelpersCoreDataStack. Call with caution.
 
 @return TestHelpersCoreDataStack with an @c NSInMemoryStoreType for testing
 */
+ (TestHelpersCoreDataStack *)inMemoryCoreDataStack;

/*!
 Create and return an in memory @c NSManagedObjectContext
 
 @see inMemoryCoreDataStack
 
 @return an in memory @c NSManagedObjectContext
 */
+ (NSManagedObjectContext *)inMemoryContext;
@end
