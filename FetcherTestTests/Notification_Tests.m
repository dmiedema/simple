//
//  Notification_Tests.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "TestHelpers.h"
#import "Notification+Accessors.h"

@interface Notification_Tests : XCTestCase
@property (strong, nonatomic) NSManagedObjectContext *context;
@end

@implementation Notification_Tests

- (void)testCreateOrUpdateCreates {
    XCTAssertNotNil([Notification createOrUpdate:_TestHelpersNotificationDictionary() inContext:self.context], @"createOrUpdate should always return a notification if valid data is given");
}

- (void)testLoadByNotificationID {
    NSDictionary *data = _TestHelpersNotificationDictionary();
    Notification *created = [Notification createOrUpdate:data inContext:self.context];
    
    Notification *loaded = [Notification loadByNotificationID:data[@"id"] context:self.context];
    
    XCTAssertTrue([created.udid isEqualToString:loaded.udid], @"Created ID should be equal to loaded ID");
    
    XCTAssertEqualObjects(loaded, created, @"created & loaded should be equal objects");
}

- (void)testShouldBeDismissed {
    NSDictionary *data = _TestHelpersNotificationDictionary();
    Notification *notification = [Notification createOrUpdate:data inContext:self.context];
    
    [notification dismissNotification];
    
    XCTAssertTrue([notification shouldBeDismissed], @"notification should be dismissed");
}

- (void)testShouldNotBeDismissed {
    NSMutableDictionary *data = [_TestHelpersNotificationDictionary() mutableCopy];
    Notification *notification = [Notification createOrUpdate:data inContext:self.context];
    
    [notification dismissNotification];
    
    XCTAssertTrue([notification shouldBeDismissed], @"notification should be dismissed");
    
    data[@"modification_date"] = @([[NSDate date] timeIntervalSince1970]);
    
    notification = [Notification createOrUpdate:data inContext:self.context];
    
    XCTAssertFalse([notification shouldBeDismissed], @"With later modification date, notification should not be dismissed.");
    XCTAssertTrue([notification hasBeenUpdated], @"Notification should say it has been updated");
}

#pragma mark - Setup
- (void)setUp {
    [super setUp];
    self.context = [TestHelpers inMemoryContext];
}

- (void)tearDown {
    [super tearDown];
}

@end
