//
//  NSArray+SKFit_Tests.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "NSArray+SFKit.h"

@interface NSArray_SKFit_Tests : XCTestCase

@end

@implementation NSArray_SKFit_Tests

- (void)testIsEmptyWhenEmpty {
    XCTAssertTrue([@[] isEmpty], @"Empty array should be empty");
}

- (void)testIsNotEmptyWhenNot {
    XCTAssertFalse([@[[NSObject new]] isEmpty], @"Not empty array should not be empty");
}

#pragma mark - Setup
- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
