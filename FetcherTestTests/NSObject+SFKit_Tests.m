//
//  NSObject+SFKit_Tests.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NSObject+SFKit.h"

@interface NSObject_SFKit_Tests : XCTestCase

@end

@implementation NSObject_SFKit_Tests

- (void)testNilIsNil {
    XCTAssertNil(sf_ObjectOrNull(nil), @"Nil should still be nil");
}

- (void)testNSNullIsNil {
    XCTAssertNil(sf_ObjectOrNull([NSNull null]), @"[NSNull null] should be nil");
}

- (void)testObjectIsNotNil {
    XCTAssertNotNil(sf_ObjectOrNull([NSObject new]), @"[NSObject new] should not be nil");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        NSObject *obj = [NSNull null];
        XCTAssertNil(sf_ObjectOrNull(obj), @"obj should evaluate to nil");
        XCTAssertNotNil([NSObject new], @"[NSObject new] should evaluate to not nil");
    }];
}

#pragma mark - Setup
- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


@end
