//
//  NSString+SFKit_Tests.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NSString+SFKit.h"

@interface NSString_SFKit_Tests : XCTestCase

@end

@implementation NSString_SFKit_Tests

- (void)testStripsHTMLTags {
    NSString *html = @"<b>html</b>";
    NSString *notHTML = @"html";
    
    XCTAssertTrue([[html sf_stringByStrippingHTML] isEqualToString:notHTML], @"stripped HTML should equal '%@'", notHTML);
}

#pragma mark - Setup
- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


@end
