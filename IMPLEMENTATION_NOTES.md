# Implementation Notes

## Debugging Issue

- I was having issues with the endpoint so I modified the `JSON` and saved in in _jsonOutput.json_. I used `python -m SimpleHTTPServer` to run a local webserver so I could 'fetch' my parsing `JSON`.

- I tried my best to validate/convert the json response that had invalid characters/parse errors. I decided to wrap those calls in a macro so that they could easily be removed since I only want to do it on invalid JSON. What it does is bring the JSON in as a string and strip known invalid instances from the string (super niave parser) and convert that to a JSON array to pass to the completion block.

## Design Decisions

- I abstracted the `UITableViewDataSource` out of the main `UITableViewController`, I like that it makes the `UITableViewController` smaller. I feel it helps show a separation of responsibility however `UITableView` `Datasource`/`Delegate` seem so tightly coupled to `UITableViewController` it doesn't always seem to help as much as I'd like it to.

- I decided to clear the cache when fetching messages since that method is called whether a user initiated fetch or an automated 1 minute fetch.

- I'm still not 100% on my handling of async network calls and the abstracted data source but it seems to work, I'm totally open to suggestions on it.

- I went over board and just had fun on the empty view.

- I opted for a visual representation of the level instead of listing it in the cell. Also put in an indicator for a message that had been previously dismissed but has been updated since dismissal and is now being shown.

- I mostly used autolayout for the visual items, 1 for the cell so it would automatically measure size when wrapping (hopefully) and also for the empty message view. Mostly out of my comfort level with autolayout at this point. I did try to mix in IB autolayout, programatic autolayout and (super trivial) manual frame setting on views.

- I opted to use some categories for a few shared functionality type things. I like using a `UIColor` category for storing application specific colors. Also found a nice helper to strip HTML characters off of NSStrings for setting the body preview text.

- I didn't write as many tests as I normally would since I didn't include `OCMock` for mocking delegate calls and such.

- The Macro to attempt to validate the JSON is not my favorite thing however it does seem to handle malformed JSON well enough. Probably doesn't work the best under all use cases though since its very specific about what it looks for.

## API thoughts

### Drawbacks

- Having to manually mark & check message time stamps isn't my favorite thing, having no way to tell the server I've read/dismissed a message doesn't seem scalable at a few hundred messages nor that user friendly. If for some reason local storage got messed up all dismissed notifications are lost. Also there isn't a way to sync that across devices without either iCloud syncing the messages or the API telling me they've been read. Relying on iCloud could lead to stale data in the message or a potential conflict between iCloud data & API data if it a message got updated after an iCloud sync but iCloud had not been updated yet.

- Some way to tell the API a dismissed time or something so I can pass a flag to not include dismissed messages from my messages fetch would be something I'd probably add/want to add.

### Benefits

- All of my messages and contents are available on my device and network connectivity doesn't need to happen for me to 'mark' messages as read/dismissed.

- One fetch (essentially, ignoring the refetching rule) is all I need to get my data.
