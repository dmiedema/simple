//
//  NSArray+SFKit.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "NSArray+SFKit.h"

@implementation NSArray (SFKit)

- (BOOL)isEmpty {
    return self.count == 0;
}
@end
