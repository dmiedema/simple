//
//  UIColor+SFKit.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import UIKit;
/*!
 Because I'm lazy with color creation & I really like this solution
 */
@interface UIColor (SFKit)

/*!
 Helper to automatically divide colors by 255
 
 @param red value between 0 & 255.0 to divide by @c 255.0
 @param green value between 0 & 255.0 to divide by @c 255.0
 @param blue value between 0 & 255.0 to divide by @c 255.0
 @param alpha value to use for alpha
 @return the color
 */
+ (instancetype)sf_colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

/*!
 Warning Message level color
 */
+ (instancetype)warningColor;

/*!
 Info Message level color
 */
+ (instancetype)infoColor;

/*!
 Critical Message level color
 */
+ (instancetype)criticalColor;
@end
