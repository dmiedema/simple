//
//  NSString+SFKit.h
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import Foundation;

@interface NSString (SFKit)

/*!
 Strip out all html tags and their contents from a string.
 
 @see http://stackoverflow.com/questions/277055/remove-html-tags-from-an-nsstring-on-the-iphone
 
 @return an @c NSString instance with all html tags removed
 */
- (instancetype)sf_stringByStrippingHTML;
@end
