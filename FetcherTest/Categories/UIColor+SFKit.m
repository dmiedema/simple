//
//  UIColor+SFKit.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "UIColor+SFKit.h"

@implementation UIColor (SFKit)

+ (instancetype)sf_colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha {
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha];
}

+ (instancetype)warningColor {
    static UIColor *warning = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        warning = [UIColor sf_colorWithRed:239 green:246 blue:52 alpha:0.4];
    });
    return warning;
}

+ (instancetype)infoColor {
    static UIColor *info = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        info = [UIColor colorWithRed:44 green:149 blue:194 alpha:0.4];
    });
    return info;
}

+ (instancetype)criticalColor {
    static UIColor *critical = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        critical = [UIColor sf_colorWithRed:239 green:77 blue:46 alpha:0.4];
    });
    return critical;
}

@end
