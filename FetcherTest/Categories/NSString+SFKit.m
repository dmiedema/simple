//
//  NSString+SFKit.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "NSString+SFKit.h"

@implementation NSString (SFKit)

- (instancetype)sf_stringByStrippingHTML {
    NSRange range;
    NSString *copy = [self copy];
    while ((range = [copy rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
        copy = [copy stringByReplacingCharactersInRange:range withString:@""];
    }
    return copy;
}

@end
