//
//  NSArray+SFKit.h
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import Foundation;

@interface NSArray (SFKit)
/*!
 Check to see if a given array is empty
 
 @return @c YES if the array is empty, @c NO otherwise.
 */
- (BOOL)isEmpty;

@end
