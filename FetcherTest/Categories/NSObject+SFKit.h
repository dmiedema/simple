//
//  NSObject+SFKit.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import Foundation;

/*!
 Helper to check to see if an object is @c nil or @c [NSNull @c null]
 
 @param obj to check
 @return @c id the object if not nil or of type @c [NSNull @c null].  @c nil otherwise.
 */
id sf_ObjectOrNull(id obj);

@interface NSObject (SFKit)
@end
