//
//  NSObject+SFKit.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "NSObject+SFKit.h"

id sf_ObjectOrNull(id obj) {
    if (!obj || [obj isEqual:[NSNull null]]) {
        return nil;
    }
    return obj;
}

@implementation NSObject (SFKit)

@end
