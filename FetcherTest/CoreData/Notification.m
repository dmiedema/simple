//
//  Notification.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "Notification.h"


@implementation Notification

@dynamic udid;
@dynamic level;
@dynamic body;
@dynamic subject;
@dynamic created_at;
@dynamic updated_at;
@dynamic user_dimissed;
@dynamic dismissed_at;

@end
