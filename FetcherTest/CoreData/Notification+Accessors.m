//
//  Notification+Accessors.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "Notification+Accessors.h"
#import "NSObject+SFKit.h"

static NSString *EntityName = @"Notification";

@implementation Notification (Accessors)
#pragma mark - Public
+ (instancetype)createOrUpdate:(NSDictionary *)data inContext:(NSManagedObjectContext *)context {
    Notification *notification = [self loadByNotificationID:data[@"id"] context:context];
    
    if (!notification) {
        notification = [NSEntityDescription insertNewObjectForEntityForName:EntityName inManagedObjectContext:context];
        notification.udid = data[@"id"];
    }
    
    // Ignore warnings about assignment from '?:'
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
    notification.body    = sf_ObjectOrNull(data[@"body"])    ?: notification.body    ?: @"";
    notification.level   = sf_ObjectOrNull(data[@"level"])   ?: notification.level   ?: @"info";
    notification.subject = sf_ObjectOrNull(data[@"subject"]) ?: notification.subject ?: @"";
#pragma clang diagnostic pop
    
    // Dividing by 1000 since they're millisecond time stamps
    if (sf_ObjectOrNull(data[@"creation_date"])) {
        double createdAt = [data[@"creation_date"] doubleValue] / 1000;
        notification.created_at = [NSDate dateWithTimeIntervalSince1970:createdAt];
    }
    
    if (sf_ObjectOrNull(data[@"modification_date"])) {
        double updatedAt = [data[@"modification_date"] doubleValue] / 1000;
        notification.updated_at = [NSDate dateWithTimeIntervalSince1970:updatedAt];
    }
    
    return notification;
}

- (BOOL)shouldBeDismissed {
    return [self.user_dimissed boolValue] && ![self hasBeenUpdated];
}

- (BOOL)hasBeenUpdated {
    return [[self.dismissed_at laterDate:self.updated_at] isEqualToDate:self.updated_at];
}

- (void)dismissNotification {
    self.user_dimissed = @YES;
    self.dismissed_at = [NSDate date];
}

+ (instancetype)loadByNotificationID:(NSString *)notificationID context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:EntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"udid = %@", notificationID];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        if (error) {
            NSLog(@"[ERROR]: %@ %@", error, error.userInfo);
        }
        return nil;
    }
    
    return fetchedObjects.firstObject;
}
@end
