//
//  Notification.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import Foundation;
@import CoreData;


@interface Notification : NSManagedObject

@property (nonatomic, retain) NSString * udid;
@property (nonatomic, retain) NSString * level;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSNumber * user_dimissed;
@property (nonatomic, retain) NSDate * dismissed_at;
@end
