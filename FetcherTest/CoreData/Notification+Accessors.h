//
//  Notification+Accessors.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "Notification.h"

@interface Notification (Accessors)
#pragma mark - Class Methods
/*!
 @abstract Generic method to create or update a specific notification in a given context
 
 @discussion This uses @c loadByNotificationID: to see if a notification exists already.
 If one does, it loads that one, updates the properties with the @c data dictionary if it exists.
 If the node does not exist in @c data it uses the current value if one is set. Finally it will fall
 back to a default if neither are available.
 If no object is found by @c loadByNotificationID: an object is created
 
 @param data    @c NSDictionary to parse & use for the data
 @param context CoreData context to create/search for the notification
 @return created / updated notification object
 */
+ (instancetype)createOrUpdate:(NSDictionary *)data inContext:(NSManagedObjectContext *)context;

/*!
 @abstract load a particular notification by a given notification id.
 
 @param notificationID notification ID to load by
 @param context        CoreData context to search
 @return the notification if found, @c nil if it was not found.
 */
+ (instancetype)loadByNotificationID:(NSString *)notificationID context:(NSManagedObjectContext *)context;

#pragma mark - Instance Methods
/*!
 Check to see if the current notification should be dismissed.
 
 @note this checks @c user_dismissed & @c updated_at. If @c updated_at is later than @c dismissed_at this notification should still be shown. Otherwise it will not show.
 
 @return @c YES if should be hidden, @c NO otherwise
 */
- (BOOL)shouldBeDismissed;

/*!
 Check to see if the current notification has been updated since it was dismissed
 
 @return @c YES if has been updated since dismissal, @c NO otherwise
*/
- (BOOL)hasBeenUpdated;

/*!
 Set the notification to dismissed.
 
 This sets @c user_dismissed to @c YES & @c dismissed_at to @c [NSDate @c date]
 */
- (void)dismissNotification;

@end
