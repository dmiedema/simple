//
//  SFFetcherTestNotificationManager.h
//  FetcherTest
//
//  Created by Michael Distefano on 10/28/14.
//  Copyright (c) 2014 Simple. All rights reserved.
//

@import Foundation;
#import <AFNetworking/AFNetworking.h>

typedef void (^SFFetcherCompletionHandler)(NSArray *results, BOOL success);

@interface SFFetcherTestNotificationManager : AFHTTPSessionManager

/**
 * Singleton instance of `SFFetcherTestNotificationManager`.
 * All methods should be called on `[SFFetcherTestNotificationManager manager]`
 */
+ (SFFetcherTestNotificationManager *)manager;

/**
 * Fetches notifications from the server and returns them in a completion handler.
 *
 * Set a `SFFetcherCompletionHandler` to access the fetched notifications.
 *
 * Only those notifications that should be displayed (i.e. those that haven't
 * been dismissed) should be returned.
 */
- (void)fetchNotificationsWithCompletion:(SFFetcherCompletionHandler)completion;

/**
 * Dismisses a notification. 
 *
 * Dismissed notification state should be preserved after the app
 * is removed from memory.
 */
- (void)dismissNotification:(NSDictionary *)notification;

@end
