//
//  SFEmptyView.m
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "SFEmptyView.h"

@interface SFEmptyView()
@property (strong, nonatomic) UILabel *textLabel;
@property (strong, nonatomic) UILabel *tadaLabelLeft;
@property (strong, nonatomic) UILabel *tadaLabelRight;
@property (strong, nonatomic) UILabel *thumbsupLabelLeft;
@property (strong, nonatomic) UILabel *thumbsupLabelRight;
@end

@implementation SFEmptyView

#pragma mark - Init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor orangeColor];
        [self addSubview:self.textLabel];
        [self addSubview:self.tadaLabelLeft];
        [self addSubview:self.tadaLabelRight];
        [self addSubview:self.thumbsupLabelLeft];
        [self addSubview:self.thumbsupLabelRight];
        
        [self addConstraints:[self textLabelConstraints]];
        [self addConstraints:[self tadaLabelLeftContraints]];
        [self addConstraints:[self tadaLabelRightContraints]];
        [self addConstraints:[self thumbsupLabelLeftContraints]];
        [self addConstraints:[self thumbsupLabelRightContraints]];
    }
    return self;
}

#pragma mark - Getters
#pragma mark Views
- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 60)];
        _textLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.text = NSLocalizedString(@"No Messages!", @"No Messages!");
        [_textLabel sizeToFit];
        _textLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _textLabel;
}

- (UILabel *)tadaLabelLeft {
    if (!_tadaLabelLeft) {
        _tadaLabelLeft = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _tadaLabelLeft.text = @"🎉";
        _tadaLabelLeft.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _tadaLabelLeft;
}

- (UILabel *)tadaLabelRight {
    if (!_tadaLabelRight) {
        _tadaLabelRight = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _tadaLabelRight.text = @"🎉";
        _tadaLabelRight.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _tadaLabelRight;
}

- (UILabel *)thumbsupLabelLeft {
    if (!_thumbsupLabelLeft) {
        _thumbsupLabelLeft = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _thumbsupLabelLeft.text = @"👍";
        _thumbsupLabelLeft.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _thumbsupLabelLeft;
}

- (UILabel *)thumbsupLabelRight {
    if (!_thumbsupLabelRight) {
        _thumbsupLabelRight = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _thumbsupLabelRight.text = @"👍";
        _thumbsupLabelRight.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _thumbsupLabelRight;
}

#pragma mark Constraints
- (NSArray *)textLabelConstraints {
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.textLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.textLabel.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.textLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.textLabel.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    
    return @[centerX, centerY];
}

- (NSArray *)tadaLabelLeftContraints {
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.tadaLabelLeft attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.textLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.tadaLabelLeft attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.textLabel attribute:NSLayoutAttributeLeft multiplier:1 constant:-8];
    return @[centerY, trailing];
}

- (NSArray *)tadaLabelRightContraints {
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.tadaLabelRight attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.textLabel attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.tadaLabelRight attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.textLabel attribute:NSLayoutAttributeRight multiplier:1 constant:8];
    return @[centerY, leading];
}

- (NSArray *)thumbsupLabelLeftContraints {
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.thumbsupLabelLeft attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tadaLabelLeft attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.thumbsupLabelLeft attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.tadaLabelLeft attribute:NSLayoutAttributeLeft multiplier:1 constant:-8];
    return @[centerY, trailing];
}

- (NSArray *)thumbsupLabelRightContraints {
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.thumbsupLabelRight attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tadaLabelRight attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.thumbsupLabelRight attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.tadaLabelRight attribute:NSLayoutAttributeRight multiplier:1 constant:8];
    return @[centerY, leading];
}

#pragma mark - Implementation
- (void)showEmptyView {
    [self layoutIfNeeded];
    
    CGAffineTransform tadaScale  = CGAffineTransformMakeScale(3.5, 3.5);
    
    CGAffineTransform thumbsUpRotate = CGAffineTransformMakeRotation(-M_PI);
    CGAffineTransform thumbsUpScale  = CGAffineTransformMakeScale(0.5, 0.5);
    
    self.textLabel.transform          = CGAffineTransformMakeScale(0.1, 0.1);
    self.tadaLabelLeft.transform      = tadaScale;
    self.tadaLabelRight.transform     = tadaScale;
    self.thumbsupLabelLeft.transform  = CGAffineTransformConcat(thumbsUpScale, thumbsUpRotate);
    self.thumbsupLabelRight.transform = CGAffineTransformConcat(thumbsUpScale, thumbsUpRotate);
    
    self.alpha = 0.0;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
    }];
    
    /* Supress warning about enum assignment out of range. */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wassign-enum"
    [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:1.3 options:(UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse) animations:^{
#pragma clang diagnostic pop
        self.textLabel.transform          = CGAffineTransformIdentity;
        self.tadaLabelLeft.transform      = CGAffineTransformIdentity;
        self.tadaLabelRight.transform     = CGAffineTransformIdentity;
        self.thumbsupLabelLeft.transform  = CGAffineTransformIdentity;
        self.thumbsupLabelRight.transform = CGAffineTransformIdentity;
        
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

- (void)hideEmptyView {
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    }];
}

@end
