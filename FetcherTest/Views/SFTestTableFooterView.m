//
//  SFTestTableFooterView.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "SFTestTableFooterView.h"

@interface SFTestTableFooterView()
/// Label to hold when last updated
@property (strong, nonatomic) UILabel *updatedAtLabel;
/// date formatter to take updated date and convert it to a string
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation SFTestTableFooterView

#pragma mark - Init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _updatedAtLabel = [[UILabel alloc] initWithFrame:frame];
        _updatedAtLabel.textAlignment = NSTextAlignmentCenter;
        _updatedAtLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        
        [self addSubview:_updatedAtLabel];
    }
    return self;
}

#pragma mark - Implementation
- (void)setUpdatedTime:(NSDate *)updatedAt {
    if (!updatedAt) { updatedAt = [NSDate date]; }
    
    self.updatedAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Updated at: %@", @"Updated at: X"), [self.dateFormatter stringFromDate:updatedAt]];
}

- (void)errorUpdating {
    self.updatedAtLabel.text = NSLocalizedString(@"Error Updating Notifications", @"Error Updating Notifications");
}

#pragma mark - Getters
- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"hh:mm a " options:0 locale:[NSLocale currentLocale]];
    }
    return _dateFormatter;
}

@end
