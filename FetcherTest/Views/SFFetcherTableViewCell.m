//
//  SFFetcherTableViewCell.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "SFFetcherTableViewCell.h"
#import "Notification+Accessors.h"
#import "UIColor+SFKit.h"
#import "NSString+SFKit.h"

@interface SFFetcherTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *dismissedIndicator;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

@end


@implementation SFFetcherTableViewCell

- (void)awakeFromNib {
    // Make the dismissed Indicator round
    self.dismissedIndicator.layer.cornerRadius = CGRectGetHeight(self.dismissedIndicator.frame) / 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setupCell:(Notification *)notification {
    self.subjectLabel.text = notification.subject;
    self.bodyLabel.text = [notification.body sf_stringByStrippingHTML];
    
    self.dismissedIndicator.hidden = ![notification hasBeenUpdated];
    [self setBackgroundForLevel:notification.level];
}

#pragma mark - Private
- (void)setBackgroundForLevel:(NSString *)level {
    if ([level isEqualToString:@"info"]) {
        self.contentView.backgroundColor = [UIColor infoColor];
    } else if ([level isEqualToString:@"critical"]) {
        self.contentView.backgroundColor = [UIColor criticalColor];
    } else if ([level isEqualToString:@"warning"]) {
        self.contentView.backgroundColor = [UIColor warningColor];
    }
}

@end
