//
//  SFFetcherTableViewCell.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import UIKit;
@class Notification;

@interface SFFetcherTableViewCell : UITableViewCell
/*!
 Tell the cell to setup its data as necessary
 
 @param notification notification to use for setup.
 */
- (void)setupCell:(Notification *)notification;
@end
