//
//  SFTestTableFooterView.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import UIKit;

@interface SFTestTableFooterView : UIView
/*!
 Set the last updated time for the footer to show
 
 @warning updatedAt can not be nil
 
 @param updatedAt time to use for formatting. If @c nil, @c [NSDate date] is used
 */
- (void)setUpdatedTime:(NSDate *)updatedAt;

/*!
 Have the footer show an error message
 */
- (void)errorUpdating;
@end
