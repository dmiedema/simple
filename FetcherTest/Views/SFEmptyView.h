//
//  SFEmptyView.h
//  FetcherTest
//
//  Created by Daniel on 2/7/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import UIKit;

@interface SFEmptyView : UIView

/*!
 Tell the empty view it should shown now.
 */
- (void)showEmptyView;

/*!
 Tell the empty view to hide.
 */
- (void)hideEmptyView;

@end
