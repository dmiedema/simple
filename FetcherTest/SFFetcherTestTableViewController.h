//
//  SFFetcherTestTableViewController.h
//  FetcherTest
//
//  Created by Michael Distefano on 10/28/14.
//  Copyright (c) 2014 Simple. All rights reserved.
//

@import UIKit;

@interface SFFetcherTestTableViewController : UITableViewController
/// Managed object context to do core data fetches with
@property (strong, nonatomic) NSManagedObjectContext *context;
@end
