//
//  SFFetcherTestTableViewController.m
//  FetcherTest
//
//  Created by Michael Distefano on 10/28/14.
//  Copyright (c) 2014 Simple. All rights reserved.
//

#import "SFFetcherTestTableViewController.h"
#import "SFFetcherTestNotificationManager.h"

#import "SFFetcherTestTableViewDataSource.h"

#import "SFFetcherTableViewCell.h"
#import "SFTestTableFooterView.h"

#import "SFMessageViewController.h"
#import "SFEmptyView.h"

#import "Notification+Accessors.h"
#import "NSArray+SFKit.h"

#import "AppDelegate.h"

@interface SFFetcherTestTableViewController () <SFFetcherTestTableViewDataSourceProtocol>
@property (strong, nonatomic) SFFetcherTestTableViewDataSource *dataSource;
@property (strong, nonatomic) SFTestTableFooterView *footerView;
@property (strong, nonatomic) SFEmptyView *emptyView;
@property (strong, nonatomic) NSTimer *fetchTimer;
@end

static NSTimeInterval kSFTimerOneMinute = 60; // 60 seconds == minute
@implementation SFFetcherTestTableViewController
#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Support pull to refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadNotifications) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    // Attempt to use auto measuring cells
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SFFetcherTableViewCell" bundle:nil] forCellReuseIdentifier:NotificationCellReuseIdentifer];
    
    self.tableView.tableFooterView = self.footerView;
    
    self.tableView.dataSource = self.dataSource;
    self.dataSource.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadNotifications];
    
    self.navigationItem.title = NSLocalizedString(@"Messages", @"Messages");
    
    self.fetchTimer = [NSTimer timerWithTimeInterval:kSFTimerOneMinute target:self selector:@selector(reloadNotifications) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.fetchTimer forMode:NSDefaultRunLoopMode];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.fetchTimer invalidate];
}

#pragma mark - Implementation
- (void)reloadNotifications {
    [self.refreshControl beginRefreshing];
    [self.dataSource loadResults];
}

#pragma mark - Getters
-(SFTestTableFooterView *)footerView {
    if (!_footerView) {
        _footerView = [[SFTestTableFooterView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 40)];
    }
    return _footerView;
}

- (SFFetcherTestTableViewDataSource *)dataSource {
    if (!_dataSource) {
        _dataSource = [SFFetcherTestTableViewDataSource dataSourceWithCellConfigurationBlock:^(SFFetcherTableViewCell *cell, Notification *item) {
            // configure cell
            [cell setupCell:item];
        } managedObjectContext:self.context];
    }
    return _dataSource;
}

- (NSManagedObjectContext *)context {
    if (!_context) {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _context =  delegate.managedObjectContext;
    }
    return _context;
}

- (SFEmptyView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[SFEmptyView alloc] initWithFrame:self.view.bounds];
    }
    return _emptyView;
}
#pragma mark - TableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60; // magic height number
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = [self.dataSource itemAtIndexPath:indexPath];
    
    NSLog(@"%@", notification);
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:[SFMessageViewController messageViewControllerWithNotification:notification]];
    
    [self presentViewController:navController animated:YES completion:^{
        [notification dismissNotification];
    }];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *dismissAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Dismiss", @"Dismiss") handler:^(UITableViewRowAction *action, NSIndexPath *_indexPath) {
        Notification *notification = [self.dataSource itemAtIndexPath:_indexPath];
        if (!notification) { return; }
        
        [notification dismissNotification];
        if (![notification shouldBeDismissed]) {
            NSString *title = NSLocalizedString(@"Uh Oh", @"uh oh");
            NSString *message = NSLocalizedString(@"I can't dismiss that notification - it says its been updated in the future", @"I can't dismiss that notification - it says its been updated in the future");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Aw Man. Okay", @"Aw Man. Okay") style:UIAlertActionStyleDestructive handler:^(UIAlertAction *alertAction) {
                [alert dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        if ([self.dataSource.results isEmpty]) { [self showEmptyView]; }
    }];
    
    return @[dismissAction];
}

#pragma mark - SFFetcherTestTableViewDataSourceProtocol
- (void)dataSourceLoading {
    // show a progress hud or something
}

- (void)dataSourceLoaded {
    if (self.refreshControl.isRefreshing) { [self.refreshControl endRefreshing]; }
    [self.footerView setUpdatedTime:[NSDate date]];
    
    if ([self.dataSource.results isEmpty]) {
        [self showEmptyView];
    } else {
        [self hideEmptyView];
        [self.tableView reloadData];
    }
}

- (void)dataSourceFailedToLoad:(NSError *)error {
    if (self.refreshControl.isRefreshing) { [self.refreshControl endRefreshing]; }
    [self hideEmptyView];
    [self.footerView errorUpdating];
}

#pragma mark - Empty View
- (void)showEmptyView {
    [self.view addSubview:self.emptyView];
    [self.emptyView showEmptyView];
}

- (void)hideEmptyView {
    [self.emptyView hideEmptyView];
    [self.emptyView removeFromSuperview];
}

@end
