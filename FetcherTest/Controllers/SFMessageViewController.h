//
//  SFMessageViewController.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import UIKit;
@class Notification;

@interface SFMessageViewController : UIViewController
/// Notification to show
@property (strong, nonatomic) Notification *notification;

/*!
 Create a @c SFMessageViewController with a specified notification
 
 @param notification notification to load
 @return created MessageViewController
 */
+ (instancetype)messageViewControllerWithNotification:(Notification *)notification;
@end
