//
//  SFMessageViewController.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "SFMessageViewController.h"
#import "Notification.h"
@import WebKit;

@interface SFMessageViewController ()
@property (strong, nonatomic) WKWebView *webView;

@end

@implementation SFMessageViewController
#pragma mark - Class
+ (instancetype)messageViewControllerWithNotification:(Notification *)notification {
    SFMessageViewController *controller = [[SFMessageViewController alloc] init];
    controller.notification = notification;
    
    return controller;
}

#pragma mark - View life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.webView];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Dismiss", @"Dismiss") style:UIBarButtonItemStyleDone target:self action:@selector(dismissPressed:)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = self.notification.subject;
    [self.webView loadHTMLString:self.notification.body baseURL:nil];
}

#pragma mark - Getters
- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
    }
    return _webView;
}

#pragma mark - Implementation
- (void)dismissPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
