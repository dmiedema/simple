//
//  main.m
//  FetcherTest
//
//  Created by Michael Distefano on 10/28/14.
//  Copyright (c) 2014 Simple. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
