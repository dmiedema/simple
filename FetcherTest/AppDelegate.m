//
//  AppDelegate.m
//  FetcherTest
//
//  Created by Michael Distefano on 10/28/14.
//  Copyright (c) 2014 Simple. All rights reserved.
//

#import "AppDelegate.h"
@import CoreData;

#import "SFFetcherTestTableViewController.h"
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark - Application Life Cycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    SFFetcherTestTableViewController *fetcherTableViewController = [[SFFetcherTestTableViewController alloc] init];
    fetcherTableViewController.context = self.managedObjectContext;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:fetcherTableViewController];
    
    self.window.rootViewController = navController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    // Show status bar indicator when network activity is happening
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self saveContext];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}


#pragma mark - Core Data
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"[CoreData ERROR]: Unresolved error %@, %@", error, error.userInfo);
            abort();
        }
    }
}
#pragma mark - Core Data stack
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FetcherTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self databasePath];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:@{NSMigratePersistentStoresAutomaticallyOption:@YES,NSInferMappingModelAutomaticallyOption:@YES}
                                                           error:&error]) {
        NSLog(@"[ERROR]: Unresolved error %@, %@", error, error.userInfo);
        
        NSError *fileManagerError;
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&fileManagerError];
        if (fileManagerError) {
            NSLog(@"[ERROR]: Unable to remove file at url %@. Error - %@, %@", storeURL, fileManagerError, fileManagerError.userInfo);
            abort();
        }
        [self resetCoreDataStack];
        return self.persistentStoreCoordinator;
    }
    
    return _persistentStoreCoordinator;
}

- (void)resetCoreDataStack {
    NSLog(@"[INFO]: Resetting Core Data Stack");
    NSLog(@"[VERBOSE]: %s", __PRETTY_FUNCTION__);
    _managedObjectContext = nil;
    _managedObjectModel = nil;
    _persistentStoreCoordinator = nil;
    NSString *storePath = [[self databasePath] absoluteString];
    if ([[NSFileManager defaultManager] fileExistsAtPath:storePath]) {
        [[NSFileManager defaultManager] removeItemAtURL:[self databasePath] error:nil];
    }
}

#pragma mark - Application's Documents directory
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)databasePath {
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FetcherTests.sqlite"];
}

@end
