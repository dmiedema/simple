//
//  SFFetcherTestTableViewDataSource.h
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

@import Foundation;
@import UIKit;
@protocol SFFetcherTestTableViewDataSourceProtocol;
@class SFFetcherTableViewCell;

/// Reuse identifier for the table view cell
extern NSString * const NotificationCellReuseIdentifer;
/*!
 Cell configuration block to run when @c cellForRowAtIndexPath is called
 
 @note both params are assumed @c id's to avoid compiler warnings. Feel free to override as necessary
 
 @param cell the @c UITableViewCell subclass that will be configured
 @param item data source item to configure cell with
 */
typedef void (^SFFetcherTestTableViewCellConfigurationBlock)(id cell, id item);

/*!
 Basic @c FetcherTestTableView @c UITableViewDataSource.
 
 This is designed to make the @c SFFetcherTestTableViewController a smaller class while
 abstracting the data fetching/data source out of the controller.
 
 Instead of making @c SFFetcherTestTableViewController an @c NSFetchedResultsController
 I prefer going this route since the data is fetched from the network, parsed, then added to CoreData
 */
@interface SFFetcherTestTableViewDataSource : NSObject <UITableViewDataSource>
/// results that should be shown in the table view
@property (readonly, nonatomic) NSArray *results;
/// delegate to notify of data loaded/loading/load error events
@property (weak, nonatomic) id<SFFetcherTestTableViewDataSourceProtocol> delegate;

/*!
 @abstract create a new data source with a few convience things passed in
 
 @discussion This is the preferred way to create the data source since all things necessary & assumed are passed in as arguments
 
 @param configurationBlock @c SFFetcherTestTableViewCellConfigurationBlock to run when @c cellForRowAtIndexPath is called
 @param context            @c NSManagedObjectContext to use for adding/updating parsing the data received from the network
 @return configured @c UITableViewDataSource
 */
+ (instancetype)dataSourceWithCellConfigurationBlock:(SFFetcherTestTableViewCellConfigurationBlock)configurationBlock managedObjectContext:(NSManagedObjectContext *)context;

/*!
 Tell the data source to fetch the notifications from the API
 */
- (void)loadResults;

/*!
 Clear out current results.
 */
- (void)resetResults;

/*!
 Get the item for a specific index path.
 */
- (id)itemAtIndexPath:(NSIndexPath *)indexPath;
@end

/*!
 Protocol for the data source to be able to notify of specific network events
 that may be worth being notified of.
 */
@protocol SFFetcherTestTableViewDataSourceProtocol <NSObject>
@optional
/*!
 Notifty the delegate that current network activity is happening and we're loading
 */
- (void)dataSourceLoading;
@required
/*!
 Notify delegate of successful data load. Something like @c reloadData can be called now
 */
- (void)dataSourceLoaded;
/*!
 Notify delegate an error occured when downloading the data.
 */
- (void)dataSourceFailedToLoad:(NSError *)error;

@end
