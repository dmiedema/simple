//
//  SFFetcherTestTableViewDataSource.m
//  FetcherTest
//
//  Created by Daniel on 2/6/15.
//  Copyright (c) 2015 Simple. All rights reserved.
//

#import "SFFetcherTestTableViewDataSource.h"
#import "SFFetcherTestNotificationManager.h"
#import "Notification+Accessors.h"

@interface SFFetcherTestTableViewDataSource()
@property (strong, nonatomic) NSMutableArray *mutableResults;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (copy, nonatomic) SFFetcherTestTableViewCellConfigurationBlock cellConfigurationBlock;
@end

NSString * const NotificationCellReuseIdentifer = @"NotificationCellReuseIdentifer";

@implementation SFFetcherTestTableViewDataSource

#pragma mark - Initializer
+ (instancetype)dataSourceWithCellConfigurationBlock:(SFFetcherTestTableViewCellConfigurationBlock)configurationBlock managedObjectContext:(NSManagedObjectContext *)context {
    return [[SFFetcherTestTableViewDataSource alloc] initWithConfigurationBlock:configurationBlock managedObjectContext:context];
}

- (instancetype)initWithConfigurationBlock:(SFFetcherTestTableViewCellConfigurationBlock)configurationBlock  managedObjectContext:(NSManagedObjectContext *)context {
    self = [super init];
    if (self) {
        _mutableResults = [NSMutableArray array];
        _cellConfigurationBlock = configurationBlock;
        _context = context;
    }
    return self;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (NSInteger)self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NotificationCellReuseIdentifer forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    self.cellConfigurationBlock(cell, item);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Intentionally left blank.
    // Required to be implemented for custom actions.
}

#pragma mark - Implementation

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return self.results[(NSUInteger)indexPath.row];
}

- (void)loadResults {
    if ([self.delegate respondsToSelector:@selector(dataSourceLoading)]) {
        [self.delegate dataSourceLoading];
    }
    
    [[SFFetcherTestNotificationManager manager] fetchNotificationsWithCompletion:^(NSArray *results, BOOL success) {
        if (!success) {
            NSLog(@"[ERROR]: Failed to load notifications");
            [self.delegate dataSourceFailedToLoad:nil];
        } else {
            for (NSDictionary *data in results) {
                NSMutableArray *tmpResults = [self.mutableResults mutableCopy];
                
                Notification *notification = [Notification createOrUpdate:data inContext:self.context];
                if ([self.mutableResults containsObject:notification]) {
                    [tmpResults replaceObjectAtIndex:[self.mutableResults indexOfObjectIdenticalTo:notification] withObject:notification];
                } else {
                    [tmpResults addObject:notification];
                }
                
                self.mutableResults = tmpResults;
            }
            [self.delegate dataSourceLoaded];
        }
    }];
}

- (void)resetResults {
    [self.mutableResults removeAllObjects];
}

#pragma mark - Getters
- (NSArray *)results {
    NSMutableArray *results = [NSMutableArray array];
    
    [self.mutableResults.copy enumerateObjectsUsingBlock:^(Notification *obj, NSUInteger idx, BOOL *stop) {
        if (![obj shouldBeDismissed]) {
            [results addObject:obj];
        }
    }];
    
    return results;
}

@end
