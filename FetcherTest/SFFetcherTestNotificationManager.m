//
//  SFFetcherTestNotificationManager.m
//  FetcherTest
//
//  Created by Michael Distefano on 10/28/14.
//  Copyright (c) 2014 Simple. All rights reserved.
//

#import "SFFetcherTestNotificationManager.h"
#import "AppDelegate.h"
#import "Notification+Accessors.h"

#define ATTEMPT_TO_FIX_JSON 1

static NSString * kSFFetcherTestNotificationURLString         = @"http://cl.ly/code/110L290n2B0d/notifications.json";
static NSString * kSFFetcherTestNotificationBaseURLString     = @"http://cl.ly/code/110L290n2B0d/";
static NSString * kSFFetcherTestNotificationEndpointURLString = @"notifications.json";

@interface SFFetcherTestNotificationManager()
@property (strong, nonatomic) NSManagedObjectContext *context;
@end

@implementation SFFetcherTestNotificationManager

#pragma mark - Public
+ (SFFetcherTestNotificationManager *)manager {
    static SFFetcherTestNotificationManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SFFetcherTestNotificationManager alloc] initWithBaseURL:[NSURL URLWithString:kSFFetcherTestNotificationBaseURLString]];
    });
    return manager;
}

- (void)fetchNotificationsWithCompletion:(SFFetcherCompletionHandler)completion {
    NSLog(@"[VERBOSE]: Clearing cache & fetching");
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self GET:kSFFetcherTestNotificationURLString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//    [self GET:@"http://localhost:8000/jsonOutput.json" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"[DEBUG]: Successful GET");
        
        NSArray *jsonData;
#if ATTEMPT_TO_FIX_JSON
        /* This is used to handle the invalid json characters
         Bring the response in as Data, convert to a string
         Strip known invalid strings from said strings & convert to JSON
         */
        NSString *responseString;
        BOOL usedLossy;
        [NSString stringEncodingForData:responseObject encodingOptions:@{} convertedString:&responseString usedLossyConversion:&usedLossy];

        NSData *data = [[self strippedStringToValidJSON:responseString] dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        if (error) {
            NSLog(@"[ERROR]: Error parsing JSON: %@ -- %@", error, error.userInfo);
        }
#else
        jsonData = responseObject;
#endif
        if (completion) { completion(jsonData, YES); }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"[ERROR]: url: %@\n%@ %@", task.currentRequest.URL, error, error.userInfo);
        
        if ([task.currentRequest.URL.host isEqualToString:@"localhost"]) {
            NSLog(@"[ERROR]: Current host is expected to be `localhost`\nPlease run\n\npython -m SimpleHTTPServer\n\nTo host the file correctly.\nThis was done to fix some debugging issues.");
        }
        if (completion) { completion(nil, NO); }
    }];
}

- (void)dismissNotification:(NSDictionary *)notification {
    NSString *notificationID = notification[@"id"];
    Notification *notificationObject = [Notification loadByNotificationID:notificationID context:self.context];
    
    [notificationObject dismissNotification];
}

#pragma mark - Private
- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
#if ATTEMPT_TO_FIX_JSON
        self.responseSerializer = [AFHTTPResponseSerializer serializer];
        self.requestSerializer  = [AFHTTPRequestSerializer serializer];
#else
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer  = [AFJSONRequestSerializer serializer];
#endif
    }
    return self;
}

- (NSManagedObjectContext *)context {
    if (!_context) {
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _context = delegate.managedObjectContext;
    }
    return _context;
}

- (NSString *)strippedStringToValidJSON:(NSString *)input {
    return [[[[input stringByReplacingOccurrencesOfString:@"\n" withString:@""]
              stringByReplacingOccurrencesOfString:@"\t" withString:@""]
             stringByReplacingOccurrencesOfString:@"\"el\"" withString:@"'el'"]
            stringByReplacingOccurrencesOfString:@"\"ltr\"" withString:@"'ltr'"];
}

@end
