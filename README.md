Overview
---------------

`FetcherTest` is a project that fetches a JSON file containing notifications from a public server and displays them in a `UITableViewController`. It allows dismissal of notifications, the status of which persist across app launches.

### Structure

In the main project folder you will find the following directory structure:

````
├── AppDelegate.h
├── AppDelegate.m
├── SFFetcherTestNotificationManager.h
├── SFFetcherTestNotificationManager.m
├── SFFetcherTestTableViewController.h
├── SFFetcherTestTableViewController.m
```

### Instructions

Your task is to modify `SFFetcherTestNotificationManager` and `SFFetcherTestTableViewController` to fetch and display notifications. Notifications may be loaded at the URL specified as a constant in the `@implementation` of `SFFetcherTestNotificationManager`.

`FetcherTest` should fetch notifications from the server by using `AFNetworking`, `NSURLSession`, or `NSURLConnection`. The fetched notifications should be displayed to the user in the table view that is part of `SFFetcherTestTableViewController`. Fetched notifications should be cached for a time interval of 1 minute. You should display the last time the notifications were fetched in a table view footer.

Users should be able to dismiss notifications. Upon dismissal the notifications should be removed from the table view. Notification dismissal state should be preserved across app launches. Each notification has a unique identifier, a file creation date, and a file updated date. It is your responsibility to display only those notifications which haven't been dismissed. If a notification has been dismissed but has been updated after the user has dismissed it, it should be displayed again.

Notifications have a `subject`, `body`, and `level`. All three of these pieces of data should be communicated somehow in the cell; how this is done is up to you. Tapping on a cell should open a web view that displays the body text (it's formatted as HTML) and gives users an option to dismiss the displayed notification.

Pre-processor warnings are in place to point out where you are expected to modify the implementation. When the project is done no warnings should remain. In addition to warnings there are comments detailing how you should proceed.

### Follow-up

After you have finished implementing the aforementioned features please write a short document titled `IMPLEMENTATION_NOTES.md` in markdown format detailing some of your design decisions. Include in this file, in your words, the benefits and drawbacks of such a static notification system and if and how you would design an API differently if you were given the chance.
